import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";
import { Shipper } from '../shared/shipper';

@Component({
  selector: 'app-shipper-create',
  templateUrl: './shipper-create.component.html',
  styleUrls: ['./shipper-create.component.css']
})
export class ShipperCreateComponent implements OnInit {

  @Input() shipperDetails = new Shipper();

  constructor(
    public restApi: RestApiService, 
    public router: Router
  ) { }

  ngOnInit() { }

  addShipper() {
    this.restApi.createShipper(this.shipperDetails).subscribe((data: {}) => {
      this.router.navigate(['/shipper-list'])
    })
  }

}