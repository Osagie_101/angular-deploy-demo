# ShippersApp

## Note on Dockerfile & Jenkinsfile

The included Dockerfile can be used as a template to create a Docker image which will automate the `ng build` for a project.

The main point that will need to be changed in the Dockerfile for another project is the `COPY` line. You will need to get your project name from angular.json and replace `ShippersApp` with your project name.

The included Jenkinsfile can be used as a template to create a Jenkins pipeline which will build the angular app and deploy to an nginx container in openshift.

The main point that will need to be changed in the Jenkinsfile if using in another project, is the first two lines where project name and docker image names are defined.

A new Jenkins pipeline task can now be setup using the usual procedure.

## Angular information

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
